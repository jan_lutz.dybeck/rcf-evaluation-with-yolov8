# YOLOv8 for RCF segmentation

<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" alt=""></a></p>  

**note: The license above does not include licenses for other programms or projects used in this work. It only licenses work that was done by the author.**

## Table of contents
1. [Introduction](#introduction)
2. [Strucuture of Folder Tree](#folderstructure)
3. [Known Problems](#bugs)
4. [Publications & Links](#publications)
5. [Contributers](#contributors)
6. [Funding](#funding)

## 1. Introduction <a name="introduction"></a>

This is a repository for an YOLOv8 [2] model, that is trained for segmentation of RCF images. It includes a jupyter notebook for generated training images and the used generated dataset.  
This work was started as a part of a master proposal at TU Darmstadt, Laser- and Plasmaphysics [4].

## 2. Structure of Folder Tree <a name="folderstructure"></a>

Contained notebooks:
* `rcfEvaluation.ipynb`: Jupyter Notebook that executes the model.
* `rcfTrainingDataConstructor.ipynb`: Jupyter Notebook for generation of training images  

Other files:
* `best.pt`: File with the parameters for the model after training.
* `genData.yaml`: File that is needed to train a YOLOv8 model.
* `generatedData`: Folder with the used generated training dataset.
* `generatedExampleImages`: Folder with example images from rcfTrainingDataConstructor.ipynb.

## 4. Known Problems <a name="bugs"></a>
Predictions on colored RCF images might be worse that on greyscale images.
A basic function to get a greyscale image from a colored image is included in the evaluation notebook, but you can use any (maybe faster) function to do that.


## 5. Publications & Links <a name="publications"></a>

[1] [GitLab](https://git.rwth-aachen.de/jan_lutz.dybeck/rcf-evaluation-with-yolov8)

[2] [YOLOv8](https://doi.org/10.5281/zenodo.10537285)

[3] [numpy](https://numpy.org/doc/stable/)

[4] [Laser- and Plasmaphysics, TU Darmstadt](https://www.ikp.tu-darmstadt.de/forschung_kernphysik/gruppen_kernphysik/experiment/ag_m_roth/index.de.jsp)

## 6. Contributors <a name="contributors"></a>
Contributors include (alphabetically): 
*   J. Dybeck

## 7. Funding <a name="funding"></a>
* Started as a part of a Master Proposal at TU Darmstadt [4]
